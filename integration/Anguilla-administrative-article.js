describe('Test Anguilla administrative level 0, 1, 2 article ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/dataset/pp2WYG0BdeI7J_hLh7ZJ')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })   
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('Back to Search Results', function () {
        cy.contains('Back to Search Results').click({force: true}) 
        cy.url()
    })
    
    describe('Topics', function () {
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('global warming', function () {
            cy.contains('global warming').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=global%20warming')
        })
        it('climate change', function () {
            cy.contains('climate change').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=climate%20change')
        })
        it('inclusion', function () {
            cy.contains('inclusion').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=inclusion')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('disabilities', function () {
            cy.contains('disabilities').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=disabilities')
        })
        it('(need to add all gov departments).', function () {
            cy.contains('(need to add all gov departments).').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=%28need%20to%20add%20all%20gov%20departments%29.')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('politics', function () {
            cy.contains('politics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=politics')
        })
        it('lgbtq', function () {
            cy.contains('lgbtq').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=lgbtq')
        })
    })
    it('Using This Dataset ', function () {
        cy.contains('Using This Dataset').click({force: true})   
    }) 
    it('lintol', function () {
        cy.contains('Lintol')
    })

})