describe('Test doctors page ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/searchresults?cats=doctors')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })     /*
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    })
    it('Check-box', function () {
        cy.get('[type="checkbox"]').check()
        cy.get('[type="checkbox"]').uncheck()
    }) 
    it('Select-menu', function () {
        cy.get('select').select('Descending') 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('<<', function () {
        cy.contains('«').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=doctors')
    })
    it('‹', function () {
        cy.contains('‹').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=doctors')
    })
    it('1', function () {
        cy.contains('1').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=doctors')
    })
    it('2', function () {
        cy.contains('2').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=doctors')
    })
    it('3', function () {
        cy.contains('3').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=doctors')
    })    
    it('…', function () {
        cy.contains('…').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=doctors')
    })
    it('›', function () {
        cy.contains('›').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=doctors')
    })
    it('»', function () {
        cy.contains('»').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=doctors')
    })   *****/




/*

    describe('Diabetes ', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=doctors') 
                    cy.get('select').select('Descending')          
            })
        it('Diabetes', function () {
            cy.contains('Complications associated with diabetes (CCGOIS 2.8)').click({force: true})
            cy.url()
                .should('include', '/dataset/3J0FQW0BdeI7J_hLvKyB')
        }) 
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */





/*

    describe('Recorded Dementia Diagnoses ', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=doctors') 
                    cy.get('select').select('Descending')          
            })
        it('Recorded Dementia Diagnoses', function () {
            cy.contains('Recorded Dementia Diagnoses').click({force: true})
            cy.url()
                .should('include', '/dataset/0jgGQW0B7IJTb33tyJT7')
        }) 
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        })
        it('dentists', function () {
            cy.contains('dentists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=dentists')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })       
    })  */






/*

    describe('Suicide Factsheet Camden 2017', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=doctors') 
                    cy.get('select').select('Descending')          
            })
        it('Suicide Factsheet Camden 2017', function () {
            cy.contains('Suicide Factsheet Camden 2017').click({force: true})
            cy.url()
                .should('include', '/dataset/glSuO20Bmah2eTSCy34V')
        }) 
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('domestic violence', function () {
            cy.contains('domestic violence').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20violence')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })  
    })      */








/*

    describe('Fire statistics: Fatalities and casualties', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=doctors') 
                    cy.get('select').select('Descending')          
            })
        it('Fire statistics: Fatalities and casualties', function () {
            cy.contains('Fire statistics: Fatalities and casualties').click({force: true})
            cy.url()
                .should('include', '/dataset/b1KvO20B-jQaiMLCG_MW')
        }) 
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        }) 
        it('domestic violence', function () {
            cy.contains('domestic violence').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20violence')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })  
    })               */







/*

    describe('NI 134 - The number of emergency bed days per head of weighted population', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=doctors') 
                    cy.get('select').select('Descending')          
            })
        it('NI 134 - The number of emergency bed days per head of weighted population', function () {
            cy.contains('NI 134 - The number of emergency bed days per head of weighted population').click({force: true})
            cy.url()
                .should('include', '/dataset/2Z15QW0BdeI7J_hLya2Y')
        }) 
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })    
         it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })  
    })   */










    describe('Government Major Projects Portfolio data for the Home Office', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=doctors') 
                    cy.get('select').select('Descending')          
            })
        it('Government Major Projects Portfolio data for the Home Office', function () {
            cy.contains('Government Major Projects Portfolio data for the Home Office').click({force: true})
            cy.url()
                .should('include', '/dataset/Mp2EQW0BdeI7J_hLvq7i')
        }) 
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        })
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('recycling', function () {
            cy.contains('recycling').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=recycling')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        }) 
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })  
    })
})
