describe('Test operations page ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/searchresults?cats=operations')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })     /*
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    })
    it('Check-box', function () {
        cy.get('[type="checkbox"]').check()
        cy.get('[type="checkbox"]').uncheck()
    }) 
    it('Select-menu', function () {
        cy.get('select').select('Descending') 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('<<', function () {
        cy.contains('«').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=operations')
    })
    it('‹', function () {
        cy.contains('‹').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=operations')
    })
    it('1', function () {
        cy.contains('1').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=operations')
    })
    it('2', function () {
        cy.contains('2').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=operations')
    })
    it('3', function () {
        cy.contains('3').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=operations')
    })    
    it('…', function () {
        cy.contains('…').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=operations')
    })
    it('›', function () {
        cy.contains('›').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=operations')
    })
    it('»', function () {
        cy.contains('»').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=operations')
    })   */





/*

    describe('Egypt administrative level 1 ', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=operations') 
                    cy.get('select').select('Descending')          
            })
        it('Egypt administrative level 1 ', function () {
            cy.contains('Egypt administrative level 1 (governorate) population statistics.').click({force: true})
            cy.url()
                .should('include', '/dataset/Up3jX20BdeI7J_hLuLWg')
        }) 
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('undergraduate', function () {
            cy.contains('undergraduate').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=undergraduate')
        })
        it('squash', function () {
            cy.contains('squash').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=squash')
        })
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })
        it('further education', function () {
            cy.contains('further education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=further%20education')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })  */






    /*
    describe('Cabo Verde ', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=operations') 
                    cy.get('select').select('Descending')          
            })
        it('Cabo Verde ', function () {
            cy.contains('Cabo Verde - administrative level 0-4 2010 census population statistics, administrative level 0-2 2018 projected population statistics, and gazeteer').click({force: true})
            cy.url()
                .should('include', '/dataset/SJ24QW0BdeI7J_hLGbHg')
        }) 
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('inclusion', function () {
            cy.contains('inclusion').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=inclusion')
        }) 
        it('climate change', function () {
            cy.contains('climate change').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=climate%20change')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('ethnic group (to be added)', function () {
            cy.contains('ethnic group (to be added)').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=ethnic%20group%20%28to%20be%20added%29')
        })
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */






/*
    describe('Diabetes', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=operations') 
                    cy.get('select').select('Descending')          
            })
        it('Diabetes', function () {
            cy.contains('Complications associated with diabetes (CCGOIS 2.8)').click({force: true})
            cy.url()
                .should('include', '/dataset/3J0FQW0BdeI7J_hLvKyB')
        }) 
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('doctors', function () {
            cy.contains('doctors').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=doctors')
        }) 
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */







/*

    describe('INSO Key Data Dashboard', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=operations') 
                    cy.get('select').select('Descending')          
            })
        it('INSO Key Data Dashboard', function () {
            cy.contains('INSO Key Data Dashboard, Jan 2016 to May 2019').click({force: true})
            cy.url()
                .should('include', '/dataset/eVQaqmwBmah2eTSCPXnh')
        }) 
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('blockchain', function () {
            cy.contains('blockchain').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=blockchain')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        }) 
        it('computer', function () {
            cy.contains('computer').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=computer')
        })
        it('technology', function () {
            cy.contains('technology').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=technology')
        })
        it('recycling', function () {
            cy.contains('recycling').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=recycling')
        })
        it('online', function () {
            cy.contains('online').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=online')
        }) 
        it('plastics', function () {
            cy.contains('plastics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=plastics')
        })
        it('software', function () {
            cy.contains('software').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=software')
        })
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        })
        it('global warming', function () {
            cy.contains('global warming').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=global%20warming')
        }) 
        it('open source', function () {
            cy.contains('open source').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=open%20source')
        }) 
        it('internet', function () {
            cy.contains('internet').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=internet')
        })
        it('social media', function () {
            cy.contains('social media').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=social%20media')
        })
        it('hardware', function () {
            cy.contains('hardware').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hardware')
        }) 
        it('phone', function () {
            cy.contains('phone').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=phone')
        }) 
        it('pollution', function () {
            cy.contains('pollution').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=pollution')
        })
        it('electronic', function () {
            cy.contains('electronic').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=electronic')
        })
        it('laptop', function () {
            cy.contains('laptop').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=laptop')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        }) 
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    }) */







/*

describe('Whole of Afghanistan Assessment ', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=operations') 
                    cy.get('select').select('Descending')          
            })
        it('Whole of Afghanistan Assessment ', function () {
            cy.contains('Whole of Afghanistan Assessment - Hard to Reach Dataset - August 2018').click({force: true})
            cy.url()
                .should('include', '/dataset/w9IaqmwBz1n8x7rRpoi-')
        }) 
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('ethnic group (to be added)', function () {
            cy.contains('ethnic group (to be added)').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=ethnic%20group%20%28to%20be%20added%29')
        })
        it('lgbtq', function () {
            cy.contains('lgbtq').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=lgbtq')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        }) 
        it('equality', function () {
            cy.contains('equality').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=equality')
        })
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })
        it('transport', function () {
            cy.contains('transport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=transport')
        })
        it('technology', function () {
            cy.contains('technology').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=technology')
        })
        it('climate change', function () {
            cy.contains('climate change').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=climate%20change')
        })
        it('recycling', function () {
            cy.contains('recycling').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=recycling')
        }) 
        it('plastics', function () {
            cy.contains('plastics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=plastics')
        })
         it('disabilities', function () {
            cy.contains('disabilities').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=disabilities')
        })
        it('driver', function () {
            cy.contains('driver').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=driver')
        })
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        }) 
        it('global warming', function () {
            cy.contains('global warming').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=global%20warming')
        })
        it('race', function () {
            cy.contains('race').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=race')
        }) 
        it('open source', function () {
            cy.contains('open source').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=open%20source')
        })
         it('pollution', function () {
            cy.contains('pollution').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=pollution')
        })
        it('inclusion', function () {
            cy.contains('inclusion').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=inclusion')
        }) 
        it('cycle lanes', function () {
            cy.contains('cycle lanes').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=cycle%20lanes')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('vehicle', function () {
            cy.contains('vehicle').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=vehicle')
        }) 
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    })*/





/*

    describe('Access to Psychological Therapies', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=operations') 
                    cy.get('select').select('Descending')          
            })
        it('Access to Psychological Therapies', function () {
            cy.contains('Percentage of referrals to Improving Access to Psychological Therapies (IAPT) services which indicated a reliable improvement following completion of treatment (CCGOIS 2.11b)').click({force: true})
            cy.url()
                .should('include', '/dataset/250FQW0BdeI7J_hLuayL')
        }) 
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })  
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    }) */









/*
    describe('Company winding up and bankruptcy petition statistics', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=operations') 
                    cy.get('select').select('Descending')          
            })
        it('Company winding up and bankruptcy petition statistics', function () {
            cy.contains('Company winding up and bankruptcy petition statistics').click({force: true})
            cy.url()
                .should('include', '/dataset/zzgGQW0B7IJTb33tv5Tw')
        }) 
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('open source', function () {
            cy.contains('open source').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=open%20source')
        })
        it('technology', function () {
            cy.contains('technology').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=technology')
        })
        it('disabilities', function () {
            cy.contains('disabilities').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=disabilities')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })  
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('inclusion', function () {
            cy.contains('inclusion').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=inclusion')
        }) 
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })   
    })
 */













    describe('2012 2013-062678 Largs', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=operations') 
                    cy.get('select').select('Descending')          
            })
        it('2012 2013-062678 Largs', function () {
            cy.contains('2012 2013-062678 Largs').click({force: true})
            cy.url()
                .should('include', '/dataset/5zgGQW0B7IJTb33t55SG')
        }) 
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('disabilities', function () {
            cy.contains('disabilities').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=disabilities')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })  
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('inclusion', function () {
            cy.contains('inclusion').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=inclusion')
        }) 
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })
})