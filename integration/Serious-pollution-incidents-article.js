describe('Test SSSerious pollution incidents 2002 to 2010 article ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/dataset/CTidQW0B7IJTb33tLZhr')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })   
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('Back to Search Results', function () {
        cy.contains('Back to Search Results').click({force: true}) 
        cy.url()
    })
    
    describe('Topics', function () {
        it('policing', function () {
            cy.contains('policing').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=policing')
        })
        
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('victims', function () {
            cy.contains('victims').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=victims')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('domestic abuse', function () {
            cy.contains('domestic abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20abuse')
        })
        it('domestic violence', function () {
            cy.contains('domestic violence').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20violence')
        })
        it('child abuse', function () {
            cy.contains('child abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=child%20abuse')
        })
        it('pollution', function () {
            cy.contains('pollution').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=pollution')
        })
        it('london, city of', function () {
            cy.contains('london, city of').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=london,%20city%20of')
        })
    })
    it('email', function () {
        cy.contains('enviro.statistics@defra.gsi.gov.uk')
    })
    it('lintol', function () {
        cy.contains('Lintol')
    })

})
