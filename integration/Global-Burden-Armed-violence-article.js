describe('Test Global Burden of Armed Violence article ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/dataset/Qzh1YW0B7IJTb33tFp0j')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })   
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('Back to Search Results', function () {
        cy.contains('Back to Search Results').click({force: true}) 
        cy.url()
    })
    
    describe('Topics', function () {
        it('policing', function () {
            cy.contains('policing').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=policing')
        })
        it('child abuse', function () {
            cy.contains('child abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=child%20abuse')
        })
        it('victims', function () {
            cy.contains('victims').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=victims')
        })
        it('trafficking', function () {
            cy.contains('trafficking').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=trafficking')
        })
        it('crime', function () {
            cy.contains('crime').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=crime')
        })
        it('domestic violence', function () {
            cy.contains('domestic violence').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20violence')
        })
        it('domestic abuse', function () {
            cy.contains('domestic abuse').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=domestic%20abuse')
        })
    })
    it('Using This Dataset ', function () {
        cy.contains('Using This Dataset').click({force: true})   
    }) 
    it('lintol', function () {
        cy.contains('Lintol')
    })

})




