describe('Test health page ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/searchresults?cats=health')           
    })
    it('How it works', function () {
        cy.contains('How it works').click()   
    }) /*
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    })
    it('Check-box', function () {
        cy.get('[type="checkbox"]').check()
        cy.get('[type="checkbox"]').uncheck()
    }) 
    it('Select-menu', function () {
        cy.get('select').select('Descending') 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('<<', function () {
        cy.contains('«').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=health')
    })
    it('‹', function () {
        cy.contains('‹').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=health')
    })
    it('1', function () {
        cy.contains('1').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=health')
    })
    it('2', function () {
        cy.contains('2').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=health')
    })
    it('3', function () {
        cy.contains('3').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=health')
    })    
    it('…', function () {
        cy.contains('…').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=health')
    })
    it('›', function () {
        cy.contains('›').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=health')
    })
    it('»', function () {
        cy.contains('»').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=health')
    }) */





/*
    describe('Cabo Verde', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=health') 
                    cy.get('select').select('Descending')          
            })
        it('Cabo Verde ', function () {
            cy.contains('Cabo Verde - administrative level 0-4 2010 census population statistics, administrative level 0-2 2018 projected population statistics, and gazeteer').click({force: true})
            cy.url()
                .should('include', '/dataset/SJ24QW0BdeI7J_hLGbHg')
        }) 
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('inclusion', function () {
            cy.contains('inclusion').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=inclusion')
        })
        it('climate change', function () {
            cy.contains('climate change').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=climate%20change')
        })
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        }) 
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        }) 
        it('ethnic group', function () {
            cy.contains('ethnic group (to be added)').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=ethnic%20group%20%28to%20be%20added%29')
        })
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    }) */





/*

    describe('Egypt administrative', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=health') 
                    cy.get('select').select('Descending')          
            })
        it('Egypt administrative', function () {
            cy.contains('Egypt administrative level 1 (governorate) population statistics.').click({force: true})
            cy.url()
                .should('include', '/dataset/Up3jX20BdeI7J_hLuLWg')
        }) 
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('undergraduate', function () {
            cy.contains('undergraduate').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=undergraduate')
        })
        it('squash', function () {
            cy.contains('squash').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=squash')
        })
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })

        it('further education', function () {
            cy.contains('further education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=further%20education')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })
*/




/*
    describe('Tonga administrative', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=health') 
                    cy.get('select').select('Descending')          
            })
        it('Tonga administrative', function () {
            cy.contains('Tonga administrative level 0, 1, 2, and 3 population statistics').click({force: true})
            cy.url()
                .should('include', '/dataset/slsoX20BL3OQCrODeiRi')
        }) 
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('ethnic group ', function () {
            cy.contains('ethnic group (to be added)').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=ethnic%20group%20%28to%20be%20added%29')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        }) 
        it('plastics', function () {
            cy.contains('plastics').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=plastics')
        }) 
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('inclusion', function () {
            cy.contains('inclusion').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=inclusion')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        }) 
        it('global warming', function () {
            cy.contains('global warming').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=global%20warming')
        }) 
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */





/*
    describe('Bathymetric Survey', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=health') 
                    cy.get('select').select('Descending')          
            })
        it('Bathymetric Survey', function () {
            cy.contains('Bathymetric Survey - 1992-04-29 - Adamant Shoal').click({force: true})
            cy.url()
                .should('include', '/dataset/BFSqO20Bmah2eTSC6n7h')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('water', function () {
            cy.contains('water').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=water')
        })
        it('volunteers', function () {
            cy.contains('volunteers').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=volunteers')
        })
        it('open source', function () {
            cy.contains('open source').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=open%20source')
        })
        it('communities', function () {
            cy.contains('communities').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=communities')
        })
        it('sand', function () {
            cy.contains('sand').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sand')
        })
        it('sport', function () {
            cy.contains('sport').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=sport')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('technology', function () {
            cy.contains('technology').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=technology')
        })
        it('adventure', function () {
            cy.contains('adventure').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=adventure')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    })*/


/*
    describe('Time-series data - Mexico', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=health') 
                    cy.get('select').select('Descending')          
            })
        it('Time-series data - Mexico', function () {
            cy.contains('Time-series data for UNHCR\' s populations of concern originating from Mexico').click({force: true})
            cy.url()
                .should('include', '/dataset/HzjXX20B7IJTb33t5puEh')
        }) 
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('asylum seeker', function () {
            cy.contains('asylum seeker').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=asylum%20seeker')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('refugee', function () {
            cy.contains('refugee').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=refugee')
        })
        it('immigration', function () {
            cy.contains('immigration').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=immigration')
        })
        it('environment', function () {
            cy.contains('environment').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=environment')
        })
        it('pollution', function () {
            cy.contains('pollution').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=pollution')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    }) */



/*
    describe('Time-series data - Central African Republican ', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=health') 
                    cy.get('select').select('Descending')          
            })
        it('Time-series data - Central African Republican', function () {
            cy.contains('Time-series data for UNHCR\'s populations of concern residing in Central African Republic').click({force: true})
            cy.url()
                .should('include', '/dataset/IjjXX20B7IJTb33t7ps-')
        }) 
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('asylm seeker', function () {
            cy.contains('asylum seeker').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=asylum%20seeker')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('refugee', function () {
            cy.contains('refugee').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=refugee')
        })
        it('immigration', function () {
            cy.contains('immigration').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=immigration')
        })
        it('housing', function () {
            cy.contains('housing').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=housing')
        })
        it('shelters', function () {
            cy.contains('shelters').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=shelters')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('hostels', function () {
            cy.contains('hostels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hostels')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    }) */




/*
    describe('Indicateurs de performance par secteur', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=health') 
                    cy.get('select').select('Descending')          
            })
        it('Indicateurs de performance par secteur', function () {
            cy.contains('Indicateurs de performance par secteur').click({force: true})
            cy.url()
                .should('include', '/dataset/Rp3YX20BdeI7J_hLu7Q6')
        }) 
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    }) */






    describe('Time-series data - Curacao', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=health') 
                    cy.get('select').select('Descending')          
            })
        it('Time-series data - Curacao', function () {
            cy.contains('Time-series data for UNHCR\'s populations of concern residing in Curaçao').click({force: true})
            cy.url()
                .should('include', '/dataset/KjjYX20B7IJTb33twZv2')
        }) 
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('asylum seeker', function () {
            cy.contains('asylum seeker').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=asylum%20seeker')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('refugee', function () {
            cy.contains('refugee').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=refugee')
        })
        it('immigration', function () {
            cy.contains('immigration').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=immigration')
        })
        it('housing', function () {
            cy.contains('housing').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=housing')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('hostels', function () {
            cy.contains('hostels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hostels')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 
    })
})