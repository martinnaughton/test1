

describe('Test hospitals page ', function () {
    beforeEach(function () {
        cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals')           
    })
    it('How it works', function () {
        cy.contains('How it works').click({force: true})   
    })  /*
    it('Imagine', function () {       
        cy.contains('Data Times').click({force: true})
            cy.url()
                .should('include', '/')           
    })
    it('About', function () {       
        cy.contains('About').click({force: true})
            cy.url()
                .should('include', '/about')           
    })
    it('Support', function () {       
    cy.contains('Support').click({force: true})
        cy.url()
            .should('include', '/#the-data-times')           
    })
    it('Contact', function () {
        cy.contains('Contact')
    })
    it('0.2', function () {
        cy.contains('0.2')
    })
    it('Input-bar', function () {
        cy.get('[type="text"]').clear()
    })
    it('Search Data Times', function () {
        cy.contains('Search Data Times').click({force: true}) 
    })
    it('Check-box', function () {
        cy.get('[type="checkbox"]').check()
        cy.get('[type="checkbox"]').uncheck()
    }) 
    it('Select-menu', function () {
        cy.get('select').select('Descending') 
    }) 
    it('?', function () {
        cy.get('[role = "img"]')
    })
    it('<<', function () {
        cy.contains('«').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('‹', function () {
        cy.contains('‹').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('1', function () {
        cy.contains('1').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('2', function () {
        cy.contains('2').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('3', function () {
        cy.contains('3').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })    
    it('…', function () {
        cy.contains('…').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('›', function () {
        cy.contains('›').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    })
    it('»', function () {
        cy.contains('»').click({force: true})
        cy.url()
            .should('include', '/searchresults?cats=hospitals')
    }) */



/*
    describe('Liberia-healthsites', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Liberia-healthsites', function () {
            cy.contains('Liberia-healthsites').click({force: true})
            cy.url()
                .should('include', '/dataset/TzjZX20B7IJTb33t3ZuE')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })   
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('dentists', function () {
            cy.contains('dentists').click({force: true})
            cy.url()
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        }) 

    }) */







/*
    describe('Nepal health facilities', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Nepal health facilities', function () {
            cy.contains('Nepal health facilities').click({force: true})
            cy.url()
                .should('include', '/dataset/fTiWYW0B7IJTb33tlZ9q')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })   
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })  */








/*
    describe('Panama-healthsites', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Panama-healthsites', function () {
            cy.contains('Panama-healthsites').click({force: true})
            cy.url()
                .should('include', '/dataset/dziWYW0B7IJTb33tTp8E')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })   
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('dentists', function () {
            cy.contains('dentists').click({force: true})
            cy.url()
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })   */







/*
    describe('Rwanda-healthsites', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Rwanda-healthsites', function () {
            cy.contains('Rwanda-healthsites').click({force: true})
            cy.url()
                .should('include', '/dataset/iDiWYW0B7IJTb33trZ_I')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })   
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('dentists', function () {
            cy.contains('dentists').click({force: true})
            cy.url()
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })         */








/*
    describe('IRC Ethiopia All Emergency Responses 31 March 2019 Update', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('IRC Ethiopia All Emergency Responses 31 March 2019 Update', function () {
            cy.contains('IRC Ethiopia All Emergency Responses 31 March 2019 Update').click({force: true})
            cy.url()
                .should('include', '/dataset/T1uXYW0BL3OQCrODJitV')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })   
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */






/*

    describe('Kiribati - Demographic, Health, Education and Transport indicators', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Kiribati - Demographic, Health, Education and Transport indicators', function () {
            cy.contains('Kiribati - Demographic, Health, Education and Transport indicators').click({force: true})
            cy.url()
                .should('include', '/dataset/qDiXYW0B7IJTb33tf593')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })   
        it('a levels', function () {
            cy.contains('a levels').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=a%20levels')
        })
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })
        it('education', function () {
            cy.contains('education').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=education')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    }) */











/*
    describe('Health boundaries in the health district of Am Timan - CHAD', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Health boundaries in the health district of Am Timan - CHAD', function () {
            cy.contains('Health boundaries in the health district of Am Timan - CHAD').click({force: true})
            cy.url()
                .should('include', '/dataset/4J2XYG0BdeI7J_hLU7Zm')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })  
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        }) 
        it('operations', function () {
            cy.contains('operations').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=operations')
        })
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('waiting lists', function () {
            cy.contains('waiting lists').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20lists')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })     */











    describe('Sierra Leone update 1501 Health Facilities Nov 2014', function () {
        beforeEach(function () {
                cy.visit('https://dev.thedatatimes.com/searchresults?cats=hospitals') 
                    cy.get('select').select('Descending')          
            })
        it('Sierra Leone update 1501 Health Facilities Nov 2014', function () {
            cy.contains('Sierra Leone update 1501 Health Facilities Nov 2014').click({force: true})
            cy.url()
                .should('include', '/dataset/_VuXYG0BL3OQCrODmigJ')
        }) 
        it('hospitals', function () {
            cy.contains('hospitals').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=hospitals')
        })  
        it('dentists', function () {
            cy.contains('dentists').click({force: true})
            cy.url()
        })
        it('health', function () {
            cy.contains('health').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=health')
        }) 
        it('waiting times', function () {
            cy.contains('waiting times').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=waiting%20times')
        })
        it('gps', function () {
            cy.contains('gps').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=gps')
        })
        it('nhs', function () {
            cy.contains('nhs').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=nhs')
        })
        it('medicines', function () {
            cy.contains('medicines').click({force: true})
            cy.url()
                .should('include', '/searchresults?cats=medicines')
        })
        it('Add favourite', function () {
            cy.get('[role = "img"]')
        })
        it('Explore Data', function () {
            cy.contains('Explore Data').click()
        })
    })

})
   
